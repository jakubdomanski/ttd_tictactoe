<?php

namespace Test;

use App\TicTacToe;
use PHPUnit\Framework\TestCase;

final class TicTacToeTest extends TestCase
{

    public function testSetCheckerboardSize()
    {
        $size = 3;
        $ticTacToe = new TicTacToe($size);
        $checkerboard = $ticTacToe->getCheckerboard()->getSizeOrFail();

        $this->assertEquals($size, $checkerboard);
    }

    public function testSetO()
    {
        $ticTacToe = new TicTacToe(3);
        $ticTacToe->setO(1, 1);

        $this->assertEquals('O', $ticTacToe->getCheckerboardFieldValue(1, 1));
    }

    /**
     * @expectedException \App\Exception\FieldIsAlreadyTakenException
     */
    public function testFieldIsAlreadyTaken()
    {
        $ticTacToe = new TicTacToe(3);
        $ticTacToe->setO(1, 1);
        $ticTacToe->setO(1, 1);
    }

    public function testSetX()
    {
        $ticTacToe = new TicTacToe(3);
        $ticTacToe->setX(1, 1);

        $this->assertEquals('X', $ticTacToe->getCheckerboardFieldValue(1, 1));
    }

    public function testGetWinner()
    {
        $payload = [
            "X" => [
                ["X", "O", "O"],
                [null, "X", null],
                [null, null, "X"]
            ],
            "O" => [
                ["O", "O", "O"],
                ["X", "X", null],
                [null, null, null]
            ],
            "NULL" => [
                ["O", "X", "O"],
                ["X", "X", "O"],
                ["O", "O", "X"]
            ]
        ];

        foreach ($payload as $winner => $arr) {
            $ticTacToe = new TicTacToe(3);
            $x = 0;
            foreach ($arr as $row) {
                foreach ($row as $y => $marker) {
                    if (null === $marker) {
                        continue;
                    }
                    $ticTacToe->{"set" . $marker}($x, $y);
                }
                $x++;
            }

            if ("NULL" === $winner) {
                $winner = null;
            }

            $this->assertEquals($winner, $ticTacToe->getWinner());
            break;
        }
    }


    /**
     * @expectedException \App\Exception\FieldDoesNotExistException
     */
    public function testFieldDoesNotExist()
    {
        $ticTacToe = new TicTacToe(3);
        $ticTacToe->setO(5, 5);
    }
}
