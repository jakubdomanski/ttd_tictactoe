<?php

namespace App\CheckerBoard;

interface BoardInterface
{
    public function __construct(int $size);

    /**
     * Get value of given field or throw exception.
     *
     * @param int $x
     * @param int $y
     * @return null|string
     */
    public function getFieldValueOrFail(int $x, int $y): ?string;

    /**
     * Set value of given field or throw exception.
     *
     * @param int $x
     * @param int $y
     * @param string $marker
     */
    public function setFieldValueOrFail(int $x, int $y, string $marker): void;

    /**
     * Return size of each axis.
     *
     * @return int
     */
    public function getSizeOrFail(): int;
}