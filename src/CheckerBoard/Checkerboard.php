<?php

namespace App\CheckerBoard;

use App\Exception\BoardIsNotASquareException;
use App\Exception\FieldDoesNotExistException;
use App\Exception\FieldIsAlreadyTakenException;

final class Checkerboard implements BoardInterface
{

    /**
     * @var int Checkerboard size
     */
    private $size;

    /**
     * @var array Checkerboard store
     */
    private $store = [];

    /**
     * Initialize checkerboard.
     *
     * @param int $size
     *
     * @return void
     */
    public function __construct(int $size)
    {
        $this->size = $size;
        $this->store = array_fill(0, $size, array_fill(0, $size, null));
    }

    /**
     * Get checkerboard field value.
     *
     * @param int $x
     * @param int $y
     *
     * @return null|string
     * @throws FieldDoesNotExistException
     */
    public function getFieldValueOrFail(int $x, int $y): ?string
    {
        if (!array_key_exists($x, $this->store) || !array_key_exists($y, $this->store[$x])) {
            throw new FieldDoesNotExistException();
        }
        return $this->store[$x][$y];
    }


    /**
     * Method which fill field with passed marker.
     *
     * @param int $x
     * @param int $y
     * @param string $marker
     *
     * @return void
     * @throws FieldDoesNotExistException
     * @throws FieldIsAlreadyTakenException
     */
    public function setFieldValueOrFail(int $x, int $y, string $marker): void
    {
        if (null !== $this->getFieldValueOrFail($x, $y)) {
            throw new FieldIsAlreadyTakenException();
        }

        $this->store[$x][$y] = $marker;
    }

    /**
     * Check if board is square if so return size of board.
     *
     * @return int
     * @throws BoardIsNotASquareException
     */
    public function getSizeOrFail(): int
    {
        $countX = count($this->store);
        $countY = count($this->store[0]);

        if ($countX !== $countY) {
            throw new BoardIsNotASquareException();
        }
        return count($this->store);
    }
}