<?php

namespace App;

final class Marker
{
    /**
     * O marker
     *
     * @var string
     */
    const O = 'O';

    /**
     * X marker
     *
     * @var string
     */
    const X = 'X';
}