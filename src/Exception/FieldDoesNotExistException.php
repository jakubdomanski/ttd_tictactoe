<?php

namespace App\Exception;

final class FieldDoesNotExistException extends \Exception
{
    /**
     * @var int Exception Code
     */
    protected $code = 2;

    /**
     * @var int Exception Message
     */
    protected $message = "This field does not exist!";

    public function __construct()
    {
        parent::__construct($this->message, $this->code);
    }
}