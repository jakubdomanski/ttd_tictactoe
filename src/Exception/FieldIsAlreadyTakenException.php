<?php

namespace App\Exception;

final class FieldIsAlreadyTakenException extends \Exception
{
    /**
     * @var int Exception Code
     */
    protected $code = 3;

    /**
     * @var int Exception Message
     */
    protected $message = "This field is already taken!";

    public function __construct()
    {
        parent::__construct($this->message, $this->code);
    }
}