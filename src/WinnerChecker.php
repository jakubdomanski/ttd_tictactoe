<?php

namespace App;

use App\CheckerBoard\Checkerboard;

final class WinnerChecker
{
    /**
     * @var CheckerBoard
     */
    private $board;

    /**
     * Array of fields to compare.
     *
     * @var array
     */
    private $fieldsToCompare = [];

    public function __construct(CheckerBoard $checkerboard)
    {
        $this->board = $checkerboard;
    }

    /**
     * Get winner from given board.
     *
     * @return null|string
     * @throws Exception\BoardIsNotASquareException
     * @throws Exception\FieldDoesNotExistException
     */
    public function get(): ?string
    {
        $checkerBoardSize = $this->getCheckerboard()->getSizeOrFail();

        //determine winner vertical
        for ($x = 0; $x < $checkerBoardSize; $x++) {
            for ($y = 0; $y < $checkerBoardSize; $y++) {
                $this->addFieldToCompare($this->getCheckerboard()->getFieldValueOrFail($x, $y));
            }
            if ($this->canDetermineWinner()) {
                return $this->getFirstFieldValue();
            }
            $this->clearFieldsToCompare();
        }

        //determine winner horizontal
        for ($y = 0; $y < $checkerBoardSize; $y++) {
            for ($x = 0; $x < $checkerBoardSize; $x++) {
                $fields[] = $this->getCheckerboard()->getFieldValueOrFail($x, $y);
            }
            if ($this->canDetermineWinner()) {
                return $this->getFirstFieldValue();
            }
            $this->clearFieldsToCompare();
        }
        //determine winner diagonal
        // \
        for ($i = 0; $i < $checkerBoardSize; $i++) {
            $this->addFieldToCompare($this->getCheckerboard()->getFieldValueOrFail($i, $i));
        }
        if ($this->canDetermineWinner()) {
            return $this->getFirstFieldValue();
        }
        $this->clearFieldsToCompare();
        // /
        for ($i = 0; $i < $checkerBoardSize; $i++) {
            $this->addFieldToCompare($this->getCheckerboard()->getFieldValueOrFail($checkerBoardSize - 1 - $i, $i));
        }
        if ($this->canDetermineWinner()) {
            return $this->getFirstFieldValue();
        }
        return null;
    }

    /**
     * @return Checkerboard
     */
    private function getCheckerboard(): Checkerboard
    {
        return $this->board;
    }

    /**
     * Set fieldsToCompare variable to empty array.
     *
     * @return void
     */
    private function clearFieldsToCompare(): void
    {
        $this->fieldsToCompare = [];
    }

    /**
     * Get array of fields to compare.
     *
     * @return array
     */
    private function getFieldsToCompare(): array
    {
        return $this->fieldsToCompare;
    }

    /**
     * Add field to array of fields to compare.
     *
     * @param null|string $value
     */
    private function addFieldToCompare(?string $value): void
    {
        $this->fieldsToCompare[] = $value;
    }

    /**
     * Check if you can determine winner from given fields.
     *
     * @return bool
     */
    private function canDetermineWinner(): bool
    {
        if (count(array_unique($this->getFieldsToCompare())) === 1) {
            return true;
        }
        return false;
    }

    /**
     * Return first element of fieldsToCompareArray.
     *
     * @return string
     */
    public function getFirstFieldValue(): string
    {
        $array = $this->getFieldsToCompare();
        return $array[0];
    }

}