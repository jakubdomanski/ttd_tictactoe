<?php

namespace App;

use App\CheckerBoard\Checkerboard;
use App\Exception\FieldIsAlreadyTakenException;

class TicTacToe
{
    /**
     * @var Checkerboard
     */
    private $checkerboard;

    public function __construct(int $size)
    {
        $this->checkerboard = new Checkerboard($size);
    }

    /**
     * @return Checkerboard
     */
    public function getCheckerboard(): Checkerboard
    {
        return $this->checkerboard;
    }

    /**
     * Initialize checkerboard.
     *
     * @param int $size
     *
     * @return void
     */
    private function initializeCheckerboard(int $size): void
    {
        $this->checkerboard = array_fill(0, $size, array_fill(0, $size, null));
    }

    /**
     * Get checkerboard field value.
     *
     * @param int $x
     * @param int $y
     *
     * @return null|string
     * @throws Exception\FieldDoesNotExistException
     */
    public function getCheckerboardFieldValue(int $x, int $y): ?string
    {
        return $this->getCheckerboard()->getFieldValueOrFail($x, $y);
    }

    /**
     * Set O marker on desired field
     *
     * @param int $x X-axis
     * @param int $y Y-axis
     *
     * @return void
     * @throws Exception\FieldIsAlreadyTakenException
     * @throws Exception\FieldDoesNotExistException
     */
    public function setO(int $x, int $y): void
    {
        $this->getCheckerboard()->setFieldValueOrFail($x, $y, Marker::O);
    }

    /**
     * Set X marker on desired field
     *
     * @param int $x X-axis
     * @param int $y Y-axis
     *
     * @return void
     * @throws FieldIsAlreadyTakenException
     * @throws Exception\FieldDoesNotExistException
     */
    public function setX($x, $y): void
    {
        $this->getCheckerboard()->setFieldValueOrFail($x, $y, Marker::X);
    }

    /**
     * A method to determine winner.
     *
     * @return null|string
     * @throws Exception\BoardIsNotASquareException
     * @throws Exception\FieldDoesNotExistException
     */
    public function getWinner(): ?string
    {
        $winner = new WinnerChecker($this->getCheckerboard());
        return $winner->get();
    }
}